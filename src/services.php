<?php

use Pikabu\Framework\Authenticator\AuthenticatorInterface;
use Pikabu\Framework\Authenticator\TokenAuthenticator;
use Pikabu\Framework\PasswordEncoder\BcryptPasswordEncoder;
use Pikabu\Framework\PasswordEncoder\PasswordEncoderInterface;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Service\AccessTokenUserProvider;

date_default_timezone_set('Europe/Moscow');

/** @var ServiceLocator $serviceLocator */
$serviceLocator->set('connection', function() {
	$databasePath = dirname(__DIR__) . '/var/db.sqlite3';

	$pdo = new \PDO("sqlite:$databasePath");
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	return $pdo;
});

$serviceLocator->set(PasswordEncoderInterface::class, function() {
	return new BcryptPasswordEncoder();
});

$serviceLocator->set(AuthenticatorInterface::class, function(ServiceLocator $serviceLocator) {
	return new TokenAuthenticator(
        $serviceLocator->get(AccessTokenUserProvider::class),
        $serviceLocator->get('request')
    );
});
