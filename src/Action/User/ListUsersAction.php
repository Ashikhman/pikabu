<?php

namespace Pikabu\Action\User;

use Pikabu\Dto\UserFilterDto;
use Pikabu\Framework\ActionInterface;
use Pikabu\Framework\Http\ApiResponse;
use Pikabu\Framework\Http\Request;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;
use Pikabu\Handler\User\ListUsersHandler;

class ListUsersAction implements ActionInterface, ServiceLocatorAwareInterface {
	/**
	 * @var ListUsersHandler
	 */
	private $handler;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->handler = $serviceLocator->get(ListUsersHandler::class);
	}

	/**
	 * @inheritDoc
	 */
	public function __invoke(Request $request) {
		$dto = new UserFilterDto($request);

		return new ApiResponse(true, '', $this->handler->handle($dto));
	}
}