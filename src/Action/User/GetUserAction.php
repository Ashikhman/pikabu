<?php

namespace Pikabu\Action\User;

use Pikabu\Framework\ActionInterface;
use Pikabu\Framework\Http\ApiResponse;
use Pikabu\Framework\Http\Request;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;
use Pikabu\Handler\User\GetUserHandler;

class GetUserAction implements ActionInterface, ServiceLocatorAwareInterface {
	/**
	 * @var GetUserHandler
	 */
	private $handler;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->handler = $serviceLocator->get(GetUserHandler::class);
	}

	/**
	 * @inheritDoc
	 */
	public function __invoke(Request $request) {
		$accessToken = $request->query->get('access_token');
		if (null === $accessToken || '' === $accessToken) {
			return new ApiResponse(false, 'No access_token parameter provided.');
		}

		$user = $this->handler->handle($accessToken);
		if (null === $user) {
			return new ApiResponse(false, 'User with given access token not found.');
		}

		return new ApiResponse(true, '', $user);
	}
}
