<?php

namespace Pikabu\Action\User;

use Pikabu\Dto\UserDto;
use Pikabu\Framework\ActionInterface;
use Pikabu\Framework\Authenticator\AuthenticatorInterface;
use Pikabu\Framework\Http\ApiResponse;
use Pikabu\Framework\Http\Request;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;
use Pikabu\Handler\User\UpdateUserHandler;

class UpdateUserAction implements ServiceLocatorAwareInterface, ActionInterface {
	/**
	 * @var UpdateUserHandler
	 */
	private $handler;

	/**
	 * @var AuthenticatorInterface
	 */
	private $auth;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->handler = $serviceLocator->get(UpdateUserHandler::class);
		$this->auth    = $serviceLocator->get(AuthenticatorInterface::class);
	}

	/**
	 * @inheritDoc
	 */
	public function __invoke(Request $request) {
		$user = $this->auth->getUser();
		$dto  = new UserDto($request);

		$errors = $dto->validate();
		if (\count($errors) > 0) {
			return new ApiResponse(false, 'Validation failed.', $errors);
		}

		if (!$this->handler->handle($user['id'], $dto)) {
			return new ApiResponse(false, 'The user was not updated.');
		}

		return new ApiResponse(true, 'The user is updated.');
	}
}
