<?php

namespace Pikabu\Action\User;

use Pikabu\Dto\AuthDto;
use Pikabu\Framework\ActionInterface;
use Pikabu\Framework\Http\ApiResponse;
use Pikabu\Framework\Http\Request;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;
use Pikabu\Handler\User\AuthUserHandler;

class AuthUserAction implements ActionInterface, ServiceLocatorAwareInterface {
	/**
	 * @var AuthUserHandler
	 */
	private $handler;

	/**
	 * @param ServiceLocator $serviceLocator
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->handler = $serviceLocator->get(AuthUserHandler::class);
	}

	/**
	 * @inheritDoc
	 */
	public function __invoke(Request $request) {
		$dto = new AuthDto($request);

		$errors = $dto->validate();
		if (count($errors) > 0) {
			return new ApiResponse(false, 'Переданы некорректные данные.', $errors);
		}

		$token = $this->handler->handle($dto);
		if (null === $token) {
			return new ApiResponse(false, 'Пользователь с таким адресом электронной почты и паролем не найден.');
		}

		return new ApiResponse(true, '', [
            'access_token' => $token,
        ]);
	}
}