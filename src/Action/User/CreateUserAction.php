<?php

namespace Pikabu\Action\User;

use Pikabu\Dto\UserDto;
use Pikabu\Exception\DuplicateEmailException;
use Pikabu\Framework\ActionInterface;
use Pikabu\Framework\Http\ApiResponse;
use Pikabu\Framework\Http\Request;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;
use Pikabu\Handler\User\CreateUserHandler;

class CreateUserAction implements ActionInterface, ServiceLocatorAwareInterface {
	/**
	 * @var CreateUserHandler
	 */
	private $handler;

	/**
	 * @param ServiceLocator $serviceLocator
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->handler = $serviceLocator->get(CreateUserHandler::class);
	}

	/**
	 * @inheritDoc
	 */
	public function __invoke(Request $request) {
		$dto = new UserDto($request);

		$errors = $dto->validate();
		if (count($errors) > 0) {
			return new ApiResponse(false, 'Переданы некорректные данные.', $errors);
		}

		try {
			$user = $this->handler->handle($dto);
		} catch (DuplicateEmailException $e) {
			return new ApiResponse(false, 'Переданы некорректные данные.', [
                'email' => $e->getMessage(),
            ]);
		}

		return new ApiResponse(true, '', [
            'access_token' => $user['access_token'],
        ]);
	}
}