<?php

namespace Pikabu\Action\User;

use Pikabu\Framework\ActionInterface;
use Pikabu\Framework\Authenticator\AuthenticatorInterface;
use Pikabu\Framework\Http\ApiResponse;
use Pikabu\Framework\Http\Request;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;
use Pikabu\Handler\User\DeleteUserHandler;

class DeleteUserAction implements ServiceLocatorAwareInterface, ActionInterface {
	/**
	 * @var DeleteUserHandler
	 */
	private $handler;

	/**
	 * @var AuthenticatorInterface
	 */
	private $auth;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->handler = $serviceLocator->get(DeleteUserHandler::class);
		$this->auth    = $serviceLocator->get(AuthenticatorInterface::class);
	}

	/**
	 * @inheritDoc
	 */
	public function __invoke(Request $request) {
		$user = $this->auth->getUser();

		if (!$this->handler->handle($user['id'])) {
			return new ApiResponse(false, 'The user was not deleted.');
		}

		return new ApiResponse(true, 'The user is deleted.');
	}
}