<?php

/**
 * PSR-4 autoload function for project classes.
 *
 * @param string $class
 */
function pikabu_autoload($class) {
	$prefix       = 'Pikabu\\';
	$prefixLength = strlen($prefix);

	if (strncmp($prefix, $class, $prefixLength) !== 0) {
		return;
	}

	$baseDir       = __DIR__;
	$relativeClass = substr($class, $prefixLength);
	$filePath      = $baseDir . '/' . str_replace('\\', '/', $relativeClass) . '.php';

	require_once $filePath;
}

spl_autoload_register('pikabu_autoload');