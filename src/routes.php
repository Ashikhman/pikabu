<?php

use Pikabu\Action\User\AuthUserAction;
use Pikabu\Action\User\CreateUserAction;
use Pikabu\Action\User\DeleteUserAction;
use Pikabu\Action\User\GetUserAction;
use Pikabu\Action\User\ListUsersAction;
use Pikabu\Action\User\UpdateUserAction;
use Pikabu\Framework\Router;

/** @var Router $router */
$router
    ->post('/api/auth.php', AuthUserAction::class)
    ->post('/api/user.php', CreateUserAction::class)
    ->post('/api/list.php', ListUsersAction::class)
    ->get('/api/user.php', GetUserAction::class)
    ->put('/api/user.php', UpdateUserAction::class)
    ->delete('/api/user.php', DeleteUserAction::class)
;
