<?php

namespace Pikabu\Dto;

use Pikabu\Framework\Http\Request;

class UserDto {
	/**
	 * @var mixed
	 */
	private $name;

	/**
	 * @var mixed
	 */
	private $password;

	/**
	 * @var mixed
	 */
	private $email;

	/**
	 * @var mixed
	 */
	private $dob;

	/**
	 * @var mixed
	 */
	private $gender;

	/**
	 * @var mixed
	 */
	private $phone;

	/**
	 * @var mixed
	 */
	private $ip;

	/**
	 * @param Request $request
	 */
	public function __construct(Request $request) {
		$this->name     = $request->request->get('name');
		$this->password = $request->request->get('password');
		$this->email    = $request->request->get('email');
		$this->dob      = $request->request->get('dob');
		$this->gender   = $request->request->get('gender');
		$this->phone    = $request->request->get('phone');
		$this->ip       = $request->getClientIp();
	}

	/**
	 * Validates user data and returns errors. Empty array is returned on success.
	 *
	 * @return array
	 */
	public function validate() {
		$errors = [];

		if (null === $this->name || '' === $this->name) {
			$errors['name'] = 'Имя пользователя не должно быть пустым.';
		} elseif (strlen($this->name) > 255) {
			$errors['name'] = 'Длина значения не может превышать 255 символов.';
		}

		if (null === $this->password || '' === $this->password) {
			$errors['password'] = 'Пароль не должен быть пустым.';
		} elseif (strlen($this->password) < 6) {
			$errors['password'] = 'Длина пароля не должна быть меньше 6 символов.';
		} elseif (strlen($this->password) > 255) {
			$errors['password'] = 'Длина пароля не должна быть больше 255 символов.';
		}

		if (null === $this->email || '' === $this->email) {
			$errors['email'] = 'Адрес электронной почты не долже быть пустым.';
		} elseif (strlen($this->email) > 255) {
			$errors['email'] = 'Длина значения не должна превышать 255 символов.';
		} elseif (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = 'Значение не является адресом электронной почты.';
		}

		if (null === $this->dob || '' === $this->dob) {
			$errors['dob'] = 'Дата рождения не передана.';
		} else {
			$error = 'Дата рождения должна быть в формате YYYY-MM-DD (1995-10-10)';
			if (!preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $this->dob, $matches)) {
				$errors['dob'] = $error;
			} else {
				$date = strtotime($this->dob);
				if (false === $date) {
					$errors['dob'] = $error;
				} elseif ($date > time()) {
					$errors['dob'] = 'Дата рождения не может быть в будущем.';
				}
			}
		}

		if (null === $this->gender || '' === $this->gender) {
			$errors['gender'] = 'Пол не может быть пустым.';
		} elseif (!ctype_digit($this->gender)) {
			$errors['gender'] = 'Пол должен быть целочисленным типом.';
		} elseif ($this->gender < 0 || $this->gender > 2) {
			$errors['gender'] = 'Пол может принимать только значения: 0, 1, 2.';
		}

		if (null !== $this->phone) {
			if ('' === $this->phone) {
				$errors['phone'] = 'Номер телефона не может быть пустой строкой.';
			} elseif (strlen($this->phone) > 20) {
				$errors['phone'] = 'Длина номера телефона не может превышать 20 символов.';
			}
		}

		return $errors;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return (string) $this->name;
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return (string) $this->password;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return (string) $this->email;
	}

	/**
	 * @return \DateTime
	 */
	public function getDob() {
		return new \DateTime($this->dob . ' 00:00:00');
	}

	/**
	 * @return int
	 */
	public function getGender() {
		return (int) $this->gender;
	}

	/**
	 * @return string|null
	 */
	public function getPhone() {
		if (null === $this->phone) {
			return null;
		}

		return (string) $this->phone;
	}

	/**
	 * @return string
	 */
	public function getIp() {
		return $this->ip;
	}
}
