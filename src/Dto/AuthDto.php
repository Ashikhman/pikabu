<?php

namespace Pikabu\Dto;

use Pikabu\Framework\Http\Request;

class AuthDto {
	/**
	 * @var mixed
	 */
	private $email;

	/**
	 * @var mixed
	 */
	private $password;

	/**
	 * @param Request $request
	 */
	public function __construct(Request $request) {
		$this->email    = $request->request->get('email');
		$this->password = $request->request->get('password');
	}

	/**
	 * Validates user data and returns errors. Empty array is returned on success.
	 *
	 * @return array
	 */
	public function validate() {
		$errors = [];

		if (null === $this->email || '' === $this->email) {
			$errors['email'] = 'Адрес электронной почты не долже быть пустым.';
		} elseif (strlen($this->email) > 255) {
			$errors['email'] = 'Длина значения не должна превышать 255 символов.';
		} elseif (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = 'Значение не является адресом электронной почты.';
		}

		if (null === $this->password || '' === $this->password) {
			$errors['password'] = 'Пароль не должен быть пустым.';
		} elseif (strlen($this->password) < 6) {
			$errors['password'] = 'Длина пароля не должна быть меньше 6 символов.';
		} elseif (strlen($this->password) > 255) {
			$errors['password'] = 'Длина пароля не должна быть больше 255 символов.';
		}

		return $errors;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return (string) $this->email;
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return (string) $this->password;
	}
}
