<?php

namespace Pikabu\Dto;

use Pikabu\Framework\Http\Request;

class UserFilterDto {
	/**
	 * @var mixed
	 */
	private $gender;

	/**
	 * @var mixed
	 */
	private $age_min;

	/**
	 * @var mixed
	 */
	private $age_max;

	/**
	 * @param Request $request
	 */
	public function __construct(Request $request) {
		$this->gender  = $request->request->get('gender');
		$this->age_min = $request->request->get('age_min');
		$this->age_max = $request->request->get('age_max');
	}

	/**
	 * @return int|null
	 */
	public function getGender() {
		if (null === $this->gender) {
			return $this->gender;
		}

		return (int) $this->gender;
	}

	/**
	 * @return int|null
	 */
	public function getAgeMin() {
		if (null === $this->age_min) {
			return null;
		}

		return (int) $this->age_min;
	}

	/**
	 * @return int|null
	 */
	public function getAgeMax() {
		if (null === $this->age_max) {
			return null;
		}

		return (int) $this->age_max;
	}
}