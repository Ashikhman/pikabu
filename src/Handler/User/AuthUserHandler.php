<?php

namespace Pikabu\Handler\User;

use Pikabu\Dto\AuthDto;
use Pikabu\Framework\PasswordEncoder\PasswordEncoderInterface;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;

class AuthUserHandler implements ServiceLocatorAwareInterface {
	/**
	 * @var \PDO
	 */
	private $pdo;

	/**
	 * @var PasswordEncoderInterface
	 */
	private $passwordEncoder;

	/**
	 * @inheritdoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->pdo             = $serviceLocator->get('connection');
		$this->passwordEncoder = $serviceLocator->get(PasswordEncoderInterface::class);
	}

	/**
	 * @param AuthDto $dto
	 */
	public function handle(AuthDto $dto) {
		$stmt = $this->pdo->prepare("
            SELECT id, password
            FROM   users
            WHERE  email = :email
        ");
		$stmt->execute([
            'email' => $dto->getEmail(),
        ]);

		$user = $stmt->fetch(\PDO::FETCH_ASSOC);
		if (false === $user) {
			return null;
		}

		if (!$this->passwordEncoder->isValid($dto->getPassword(), $user['password'])) {
			return null;
		}

		$newToken = $this->generateAccessToken();
		$stmt     = $this->pdo->prepare("
            UPDATE users
            SET    access_token = :access_token
            WHERE  id = :id
        ");
		$stmt->execute([
            'id'           => $user['id'],
            'access_token' => $newToken,
        ]);

		return $newToken;
	}

	/**
	 * @return string
	 */
	private function generateAccessToken() {
		$randomBytes = openssl_random_pseudo_bytes(16);
		if (false === $randomBytes) {
			throw new \RuntimeException('Failed to generate random bytes.');
		}

		return bin2hex($randomBytes);
	}
}