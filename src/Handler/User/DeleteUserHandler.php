<?php

namespace Pikabu\Handler\User;

use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;

class DeleteUserHandler implements ServiceLocatorAwareInterface {
	/**
	 * @var \PDO
	 */
	private $pdo;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->pdo = $serviceLocator->get('connection');
	}

	/**
	 * Deletes user with the given id.
	 *
	 * @param int $id
	 *
	 * @return bool
	 */
	public function handle($id) {
		$stmt = $this->pdo->prepare('
            DELETE FROM users WHERE id = :id
        ');
		$stmt->execute([
            'id' => $id,
        ]);

		return $stmt->rowCount() > 0;
	}
}