<?php

namespace Pikabu\Handler\User;

use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;

class GetUserHandler implements ServiceLocatorAwareInterface {
	/**
	 * @var \PDO
	 */
	private $pdo;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->pdo = $serviceLocator->get('connection');
	}

	/**
	 * @param string $accessToken
	 *
	 * @return array|null
	 */
	public function handle($accessToken) {
		$stmt = $this->pdo->prepare('
            SELECT name, dob, gender, phone
            FROM   users
            WHERE  access_token = :access_token
        ');
		$stmt->execute([
            'access_token' => $accessToken,
        ]);

		$user = $stmt->fetch(\PDO::FETCH_ASSOC);

		return false !== $user ? $user : null;
	}
}