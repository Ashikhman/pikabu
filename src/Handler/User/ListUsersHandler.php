<?php

namespace Pikabu\Handler\User;

use Pikabu\Dto\UserFilterDto;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;

class ListUsersHandler implements ServiceLocatorAwareInterface {
	/**
	 * @var \PDO
	 */
	private $pdo;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->pdo = $serviceLocator->get('connection');
	}

	/**
	 * @param UserFilterDto $dto
	 *
	 * @return array
	 */
	public function handle(UserFilterDto $dto) {
		$stmt = $this->prepareStatement($dto);
		$stmt->execute();

		return (array) $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param UserFilterDto $dto
	 *
	 * @return \PDOStatement
	 */
	private function prepareStatement(UserFilterDto $dto) {
		$params = ['now' => date('Y-m-d H:i:s')];
		$where  = [];
		$sql    = "
            SELECT   id, 
                     name, 
                     gender,
                     cast(strftime('%Y.%m%d', :now) - strftime('%Y.%m%d', dob) as int) AS age
            FROM     users
            {where}
            ORDER BY id
        ";

		$gender = $dto->getGender();
		if (null !== $gender) {
			$where[]          = 'gender = :gender';
			$params['gender'] = $gender;
		}

		$ageMin = $dto->getAgeMin();
		if (null !== $ageMin) {
			$where[]           = "dob <= date(:now, '-' || :age_min || ' years')";
			$params['age_min'] = $ageMin;
		}

		$ageMax = $dto->getAgeMax();
		if (null !== $ageMax) {
			$where[]           = "dob > date(:now, '-' || :age_max || ' years')";
			$params['age_max'] = $ageMax + 1;
		}

		if (count($where) > 0) {
			$sql = str_replace('{where}', ' WHERE ' . implode(' AND ', $where), $sql);
		} else {
			$sql = str_replace('{where}', '', $sql);
		}

		$stmt = $this->pdo->prepare($sql);
		foreach ($params as $key => $value) {
			$stmt->bindValue($key, $value);
		}

		return $stmt;
	}
}
