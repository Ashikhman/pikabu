<?php

namespace Pikabu\Handler\User;

use Pikabu\Dto\UserDto;
use Pikabu\Framework\PasswordEncoder\PasswordEncoderInterface;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;

/**
 * Updates user entity.
 */
class UpdateUserHandler implements ServiceLocatorAwareInterface {
	/**
	 * @var \PDO
	 */
	private $pdo;

	/**
	 * @var PasswordEncoderInterface
	 */
	private $passwordEncoder;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->pdo             = $serviceLocator->get('connection');
		$this->passwordEncoder = $serviceLocator->get(PasswordEncoderInterface::class);
	}

	/**
	 * @param int     $id
	 * @param UserDto $dto
	 *
	 * @return bool
	 */
	public function handle($id, UserDto $dto) {
		$stmt = $this->pdo->prepare('
            UPDATE users
            SET    name = :name,
                   password = :password,
                   email = :email,
                   dob = :dob,
                   gender = :gender,
                   phone = :phone
            WHERE  id = :id
        ');
		$stmt->execute([
            'name'     => $dto->getName(),
            'password' => $this->passwordEncoder->encode($dto->getPassword()),
            'email'    => $dto->getEmail(),
            'dob'      => $dto->getDob()->format('Y-m-d'),
            'gender'   => $dto->getGender(),
            'phone'    => $dto->getPhone(),
            'id'       => $id,
        ]);

		return $stmt->rowCount() === 1 ? true : false;
	}
}