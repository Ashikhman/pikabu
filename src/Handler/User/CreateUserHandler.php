<?php

namespace Pikabu\Handler\User;

use Pikabu\Dto\UserDto;
use Pikabu\Exception\DuplicateEmailException;
use Pikabu\Framework\PasswordEncoder\PasswordEncoderInterface;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;

class CreateUserHandler implements ServiceLocatorAwareInterface {
	/**
	 * @var \PDO
	 */
	private $pdo;

	/**
	 * @var PasswordEncoderInterface
	 */
	private $passwordEncoder;

	/**
	 * @inheritdoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->pdo             = $serviceLocator->get('connection');
		$this->passwordEncoder = $serviceLocator->get(PasswordEncoderInterface::class);
	}

	/**
	 * @param UserDto $dto
	 *
	 * @return array
	 *
	 * @throws DuplicateEmailException
	 */
	public function handle(UserDto $dto) {
		$this->assertEmailNotExists($dto->getEmail());

		$accessToken = $this->generateAccessToken();

		$stmt = $this->pdo->prepare("
	        INSERT INTO users (name, email, password, dob, gender, phone, created_at, created_ip, access_token)
	        VALUES (:name, :email, :password, :dob, :gender, :phone, :created_at, :created_ip, :access_token)
	    ");
		$stmt->execute([
            'name'         => $dto->getName(),
            'email'        => $dto->getEmail(),
            'password'     => $this->passwordEncoder->encode($dto->getPassword()),
            'dob'          => $dto->getDob()->format('Y-m-d'),
            'gender'       => $dto->getGender(),
            'phone'        => $dto->getPhone(),
            'created_at'   => date('Y-m-d H:i:s'),
            'created_ip'   => $dto->getIp(),
            'access_token' => $accessToken,
        ]);

		return [
            'access_token' => $accessToken,
        ];
	}

	/**
	 * @return string
	 */
	private function generateAccessToken() {
		$randomBytes = openssl_random_pseudo_bytes(16);
		if (false === $randomBytes) {
			throw new \RuntimeException('Failed to generate random bytes.');
		}

		return bin2hex($randomBytes);
	}

	/**
	 * Asserts that given email address not exists in database.
	 *
	 * @param string $email
	 *
	 * @throws DuplicateEmailException
	 */
	private function assertEmailNotExists($email) {
		$stmt = $this->pdo->prepare("
            SELECT id
            FROM   users
            WHERE  email = :email
        ");
		$stmt->execute([
            'email' => $email,
        ]);

		if (false !== $stmt->fetch()) {
			throw new DuplicateEmailException();
		}
	}
}
