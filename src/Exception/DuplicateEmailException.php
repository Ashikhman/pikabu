<?php

namespace Pikabu\Exception;

class DuplicateEmailException extends \RuntimeException {
	/**
	 * DuplicateEmailException constructor.
	 */
	public function __construct() {
		parent::__construct('Пользователь с таким адресом электронной почты уже зарегистрирован.', 0, null);
	}
}
