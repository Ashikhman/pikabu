<?php

namespace Pikabu\Framework;

use Pikabu\Framework\Http\Request;

class Router {
	/**
	 * @var string[]
	 */
	private $routes = [];

	/**
	 * Resolves provided request based on registered actions and returns FQCN of found action.
	 *
	 * @param Request $request
	 *
	 * @return string|null
	 */
	public function resolve(Request $request) {
		$key = strtolower(sprintf('%s|%s', $request->getMethod(), $request->getPath()));

		return isset($this->routes[$key]) ? $this->routes[$key] : null;
	}

	/**
	 * Registers GET action.
	 *
	 * @param string $uri
	 * @param string $class
	 *
	 * @return self
	 */
	public function get($uri, $class) {
		return $this->register('GET', $uri, $class);
	}

	/**
	 * Registers POST action.
	 *
	 * @param string $uri
	 * @param string $class
	 *
	 * @return self
	 */
	public function post($uri, $class) {
		return $this->register('POST', $uri, $class);
	}

	/**
	 * Registers DELETE action.
	 *
	 * @param string $uri
	 * @param string $class
	 *
	 * @return self
	 */
	public function delete($uri, $class) {
		return $this->register('DELETE', $uri, $class);
	}

	/**
	 * Registers PUT action.
	 *
	 * @param string $uri
	 * @param string $class
	 *
	 * @return self
	 */
	public function put($uri, $class) {
		return $this->register('PUT', $uri, $class);
	}

	/**
	 * Registers action with provided request settings.
	 *
	 * @param string $method
	 * @param string $uri
	 * @param string $class FQCN of an action.
	 *
	 * @return self
	 */
	public function register($method, $uri, $class) {
		$key = strtolower(sprintf('%s|%s', $method, $uri));

		$this->routes[$key] = $class;

		return $this;
	}
}
