<?php

namespace Pikabu\Framework;

use Pikabu\Framework\Authenticator\Exception\AuthenticationException;
use Pikabu\Framework\Http\ApiResponse;
use Pikabu\Framework\Http\Request;
use Pikabu\Framework\Http\Response;

/**
 * This is the entry point for the project actions.
 */
class FrontController {
	/**
	 * @var ServiceLocator
	 */
	private $serviceLocator;

	/**
	 * @var Router
	 */
	private $router;

	/**
	 * @var string
	 */
	private $srcDir;

	/**
	 * @var string
	 */
	private $projectDir;

	/**
	 * FrontController state.
	 *
	 * @var bool
	 */
	private $initialized = false;

	/**
	 * @var bool
	 */
	private $debug;

	/**
	 * FrontController constructor.
     *
     * @param bool $debug
	 */
	public function __construct($debug = false) {
		$this->debug          = $debug;
		$this->serviceLocator = new ServiceLocator();
		$this->router         = $this->serviceLocator->get(Router::class);
		$this->projectDir     = dirname(dirname(__DIR__));
		$this->srcDir         = $this->projectDir . '/src';
	}

	/**
	 * @return string
	 */
	public function getProjectDir() {
		return $this->projectDir;
	}

	/**
	 * @return string
	 */
	public function getSrcDir() {
		return $this->srcDir;
	}

	/**
	 * @return ServiceLocator
	 */
	public function getServiceLocator() {
		return $this->serviceLocator;
	}

	/**
	 * Handles given request.
	 *
	 * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
	 */
	public function handle(Request $request) {
		try {
			$this->init();

			$this->serviceLocator->set('request', function() use ($request) {
				return $request;
			});

			$actionClass = $this->router->resolve($request);
			if (null === $actionClass) {
				return new ApiResponse(false, 'Not found.');
			}

			if (!class_exists($actionClass)) {
				throw new \InvalidArgumentException("Action class `{$actionClass}` does not exist.`");
			}

			$action = $this->serviceLocator->get($actionClass);
			if (!$action instanceof ActionInterface) {
				throw new \InvalidArgumentException("Action class `{$actionClass}` must implement ActionInterface.");
			}

			return $action($request);
		} catch (AuthenticationException $e) {
			return new ApiResponse(false, $e->getMessage());
		} catch (\Exception $e) {
			if ($this->debug) {
				throw $e;
			}

			return new ApiResponse(false, 'System error.');
		}
	}

	/**
	 * Initializes FrontController.
	 */
	public function init() {
		if ($this->initialized) {
			return;
		}

		$this->registerServices();
		$this->registerRoutes();
	}

	/**
	 * Registers project service classes.
	 */
	private function registerServices() {
		$serviceLocator = $this->serviceLocator;

		include $this->srcDir . '/services.php';
	}

	/**
	 * Registers project routes.
	 */
	private function registerRoutes() {
		$router = $this->serviceLocator->get(Router::class);

		include $this->srcDir . '/routes.php';
	}
}
