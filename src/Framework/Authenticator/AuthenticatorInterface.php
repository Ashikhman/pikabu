<?php

namespace Pikabu\Framework\Authenticator;

use Pikabu\Framework\Authenticator\Exception\AuthenticationException;

interface AuthenticatorInterface {
	/**
	 * Returns current authenticated user.
	 *
	 * @return array
	 */
	public function getUser();

	/**
	 * Tries to authenticate current request's user.
	 *
	 * @return bool
	 *
	 * @throws AuthenticationException
	 */
	public function authenticate();
}
