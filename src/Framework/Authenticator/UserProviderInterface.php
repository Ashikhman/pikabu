<?php

namespace Pikabu\Framework\Authenticator;

interface UserProviderInterface {
	/**
	 * Loads user by custom parameter.
	 *
	 * @param string $value
	 *
	 * @return array|null Returns NULL if user not found.
	 */
	public function getUser($value);
}