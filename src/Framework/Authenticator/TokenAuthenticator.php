<?php

namespace Pikabu\Framework\Authenticator;

use Pikabu\Framework\Authenticator\Exception\AccessTokenNotFoundException;
use Pikabu\Framework\Authenticator\Exception\NoAccessTokenProvidedException;
use Pikabu\Framework\Http\Request;

class TokenAuthenticator implements AuthenticatorInterface {
	/**
	 * @var UserProviderInterface
	 */
	private $userProvider;

	/**
	 * @var Request
	 */
	private $request;

	/**
	 * @var array|null
	 */
	private $user;

	/**
	 * @param UserProviderInterface $userProvider
	 * @param Request               $request
	 */
	public function __construct(UserProviderInterface $userProvider, Request $request) {
		$this->userProvider = $userProvider;
		$this->request      = $request;
	}

	/**
	 * @inheritDoc
	 */
	public function getUser() {
		$this->authenticate();

		return $this->user;
	}

	/**
	 * Tries to authenticate current request's user or abort.
	 *
	 * @throws NoAccessTokenProvidedException
	 */
	public function authenticate() {
		if (null !== $this->user) {
			return;
		}

		$accessToken = $this->request->request->get('access_token');
		if (null === $accessToken) {
			$accessToken = $this->request->query->get('access_token');
		}

		if (null === $accessToken || '' === trim($accessToken)) {
			throw NoAccessTokenProvidedException::create();
		}

		$user = $this->userProvider->getUser($accessToken);
		if (null === $user) {
			throw AccessTokenNotFoundException::create();
		}

		$this->user = $user;
	}
}