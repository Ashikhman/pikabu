<?php

namespace Pikabu\Framework\Authenticator\Exception;

class AccessTokenNotFoundException extends AuthenticationException {
	/**
	 * @return AccessTokenNotFoundException
	 */
	public static function create() {
		return new self('Authentication failed: given access token not found.');
	}
}