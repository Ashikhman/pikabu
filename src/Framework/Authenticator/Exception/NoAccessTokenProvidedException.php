<?php

namespace Pikabu\Framework\Authenticator\Exception;

class NoAccessTokenProvidedException extends AuthenticationException {
	/**
	 * @return NoAccessTokenProvidedException
	 */
	public static function create() {
		return new self('Authentication required: no access_token parameter provided.');
	}
}