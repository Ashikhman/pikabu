<?php

namespace Pikabu\Framework\Http;

/**
 * Simple class that holds various request parameters and provided access to them.
 */
class ParameterBag {
	/**
	 * @var array
	 */
	private $parameters;

	/**
	 * @param array $parameters
	 */
	public function __construct(array $parameters) {
		$this->parameters = $parameters;
	}

	/**
	 * @return array
	 */
	public function all() {
		return $this->parameters;
	}

	/**
	 * @param string $name
	 *
	 * @return bool
	 */
	public function has($name) {
		return isset($this->parameters[$name]);
	}

	/**
	 * Returns parameter value with given name or default.
	 *
	 * @param string $name
	 * @param mixed  $default
	 *
	 * @return mixed
	 */
	public function get($name, $default = null) {
		return $this->has($name) ? $this->parameters[$name] : $default;
	}
}