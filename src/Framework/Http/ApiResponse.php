<?php

namespace Pikabu\Framework\Http;

/**
 * Class ApiResponse
 *
 * @package Pikabu\Framework\Http
 */
class ApiResponse extends Response {
	/**
	 * @var bool
	 */
	private $result;

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @var array
	 */
	private $data;

	/**
	 * ApiResponse constructor.
	 *
	 * @param bool   $result
	 * @param string $message
	 * @param array  $data
	 */
	public function __construct($result, $message, array $data = []) {
		parent::__construct('', 200, ['Content-Type' => 'application/json']);

		$this->result  = $result;
		$this->message = $message;
		$this->data    = $data;
	}

	/**
	 * @inheritdoc
	 */
	public function getContent() {
		return json_encode([
            'result'  => $this->result,
            'message' => $this->message,
            'data'    => $this->data,
        ], JSON_UNESCAPED_SLASHES);
	}
}