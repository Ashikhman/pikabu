<?php

namespace Pikabu\Framework\Http;

/**
 * Class containing all the information about current HTTP request.
 */
class Request {
	const METHOD_GET    = 'GET';
	const METHOD_POST   = 'POST';
	const METHOD_DELETE = 'DELETE';
	const METHOD_PUT    = 'PUT';

	/**
	 * @var ParameterBag
	 */
	public $server;

	/**
	 * @var ParameterBag
	 */
	public $headers;

	/**
	 * Request GET variables.
	 *
	 * @var ParameterBag
	 */
	public $query;

	/**
	 * Request POST variables.
	 *
	 * @var ParameterBag
	 */
	public $request;

	/**
	 * Request constructor.
	 *
	 * @param array $server
	 * @param array $get
	 * @param array $post
	 */
	public function __construct(array $server, array $get, array $post) {
		$this->server  = new ParameterBag($server);
		$this->headers = $this->collectHeaders($server);
		$this->request = new ParameterBag($post);
		$this->query   = new ParameterBag($get);
	}

	/**
	 * Creates request object based on current global variables.
	 *
	 * @return Request
	 */
	public static function createFromGlobals() {
		return new self($_SERVER, $_GET, $_POST);
	}

	/**
	 * @return string
	 */
	public function getMethod() {
		return strtoupper($this->server->get('REQUEST_METHOD', 'GET'));
	}

	/**
	 * Get request path.
	 *
	 * @return string
	 */
	public function getPath() {
		return $this->getRequestUri();
	}

	/**
	 * Returns current request URI.
	 *
	 * @return string
	 */
	public function getRequestUri() {
		return $this->server->get('REQUEST_URI', '');
	}

	/**
	 * Returns client IP.
	 *
	 * @return string
	 */
	public function getClientIp() {
		return $this->server->get('REMOTE_ADDR');
	}

	/**
	 * Collects headers from the server global variables.
	 *
	 * @param array $server
	 *
	 * @return ParameterBag
	 */
	private function collectHeaders(array $server) {
		$headers = [];
		foreach ($server as $key => $value) {
			if (strpos($key, 'HTTP_') === 0) {
				$headers[$key] = $value;
			}
		}

		return new ParameterBag($headers);
	}
}
