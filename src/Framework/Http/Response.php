<?php

namespace Pikabu\Framework\Http;

/**
 * Class represents HTTP response object.
 */
class Response {
	/**
	 * @var string
	 */
	private $content;

	/**
	 * @var int
	 */
	private $status;

	/**
	 * @var ParameterBag
	 */
	public $headers;

	/**
	 * Response constructor.
	 *
	 * @param string $content
     * @param int    $status
     * @param array  $headers
	 */
	public function __construct($content = '', $status = 200, array $headers = []) {
		$this->content = $content;
		$this->status  = $status;
		$this->headers = new ParameterBag($headers);
	}

	/**
	 * Sends request to the caller.
	 */
	public function send() {
		foreach ($this->headers->all() as $name => $value) {
		    header(sprintf('%s: %s', $name, $value));
        }

        echo $this->getContent();
	}

	/**
	 * @return string
	 */
	public function getContent() {
		return $this->content;
	}
}
