<?php

namespace Pikabu\Framework;

use Pikabu\Framework\Http\ApiResponse;
use Pikabu\Framework\Http\Request;

interface ActionInterface {
	/**
	 * @param Request $request
	 *
	 * @return ApiResponse
	 */
	public function __invoke(Request $request);
}