<?php

namespace Pikabu\Framework;

interface ServiceLocatorAwareInterface {
	/**
	 * @param ServiceLocator $serviceLocator
	 */
	public function __construct(ServiceLocator $serviceLocator);
}