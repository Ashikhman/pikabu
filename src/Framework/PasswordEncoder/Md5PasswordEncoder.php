<?php

namespace Pikabu\Framework\PasswordEncoder;

/**
 * Class uses MD5 hash algorithm for password encoding.
 */
class Md5PasswordEncoder implements PasswordEncoderInterface {
	/**
	 * @var string
	 */
	private $salt;

	/**
	 * @param string $salt
	 */
	public function __construct($salt) {
		$this->salt = $salt;
	}

	/**
	 * @inheritDoc
	 */
	public function encode($plainPassword) {
		return md5(md5($this->salt) . $plainPassword);
	}

	/**
	 * @inheritDoc
	 */
	public function isValid($plainPassword, $encodedPassword) {
		return $this->encode($plainPassword) === $encodedPassword;
	}
}