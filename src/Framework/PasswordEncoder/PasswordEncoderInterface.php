<?php

namespace Pikabu\Framework\PasswordEncoder;

interface PasswordEncoderInterface {
	/**
	 * @param string $plainPassword
	 *
	 * @return string
	 */
	public function encode($plainPassword);

	/**
	 * @param string $plainPassword
	 * @param string $encodedPassword
	 *
	 * @return bool
	 */
	public function isValid($plainPassword, $encodedPassword);
}