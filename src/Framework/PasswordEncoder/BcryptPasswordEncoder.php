<?php

namespace Pikabu\Framework\PasswordEncoder;

/**
 * Class uses BCRYPT hash algorithm for password encoding.
 */
class BcryptPasswordEncoder implements PasswordEncoderInterface {
	/**
	 * @var int
	 */
	private $cost;

	/**
	 * @param int $cost
	 */
	public function __construct($cost = 12) {
		$this->cost = $cost;
	}

	/**
	 * @inheritDoc
	 */
	public function encode($plainPassword) {
		return password_hash($plainPassword, PASSWORD_BCRYPT, [
		    'cost' => $this->cost,
        ]);
	}

	/**
	 * @inheritDoc
	 */
	public function isValid($plainPassword, $encodedPassword) {
		return password_verify($plainPassword, $encodedPassword);
	}
}