<?php

namespace Pikabu\Framework;

/**
 * Class contains all project's services.
 */
class ServiceLocator {
	/**
	 * Registered services.
	 *
	 * @var array
	 */
	private $services;

	/**
	 * Instantiated services.
	 *
	 * @var array
	 */
	private $instances;

	/**
	 * ServiceLocator constructor.
	 */
	public function __construct() {
		$this->services  = [];
		$this->instances = [];
	}

	/**
	 * Checks existence of service with given ID.
	 *
	 * @param string $id
	 *
	 * @return bool
	 */
	public function has($id) {
		return isset($this->services[$id]) || isset($this->instances[$id]);
	}

	/**
	 * @param string $id
	 *
	 * @return mixed
	 */
	public function get($id) {
		if (!isset($this->instances[$id])) {
			if (isset($this->services[$id])) {
				$this->instances[$id] = $this->services[$id]($this);
			} elseif (class_exists($id)) {
				if (is_subclass_of($id, ServiceLocatorAwareInterface::class)) {
					$this->instances[$id] = new $id($this);
				} else {
					$this->instances[$id] = new $id;
				}
			} else {
				throw new \InvalidArgumentException("Service `{$id}` was not registered or could not be autoloaded.");
			}
		}

		return $this->instances[$id];
	}

	/**
	 * @param string   $id
	 * @param callable $initializer
	 */
	public function set($id, callable $initializer) {
		$this->services[$id] = $initializer;
	}
}
