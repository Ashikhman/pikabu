<?php

namespace Pikabu\Service;

use Pikabu\Framework\Authenticator\UserProviderInterface;
use Pikabu\Framework\ServiceLocator;
use Pikabu\Framework\ServiceLocatorAwareInterface;

/**
 * Loads user by access token.
 */
class AccessTokenUserProvider implements UserProviderInterface, ServiceLocatorAwareInterface {
	/**
	 * @var \PDO
	 */
	private $pdo;

	/**
	 * @inheritDoc
	 */
	public function __construct(ServiceLocator $serviceLocator) {
		$this->pdo = $serviceLocator->get('connection');
	}

	/**
	 * @inheritDoc
	 */
	public function getUser($value) {
		$stmt = $this->pdo->prepare('
            SELECT id
            FROM   users
            WHERE  access_token = :access_token
        ');
		$stmt->execute([
            'access_token' => $value,
        ]);

		$user = $stmt->fetch(\PDO::FETCH_ASSOC);

		return false !== $user ? $user : null;
	}
}
