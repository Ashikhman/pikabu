<?php

use PHPUnit\Framework\TestCase;
use Pikabu\Framework\ServiceLocator;

class ServiceLocatorTest extends TestCase {
	/**
	 * @var ServiceLocator
	 */
	private $serviceLocator;

	public function setUp() {
		$this->serviceLocator = new ServiceLocator();
	}

	public function testGet() {
		$this->serviceLocator->set('service', function() {
			return new stdClass();
		});

		$object = $this->serviceLocator->get('service');

		$this->assertInstanceOf(stdClass::class, $object);
	}

	public function testGetReturnsSameInstantiatedObject() {
		$this->serviceLocator->set('service', function() {
			return new stdClass();
		});

		$object = $this->serviceLocator->get('service');

		$this->assertSame($object, $this->serviceLocator->get('service'));
	}

	public function testGetThrowsException() {
		$this->expectException(InvalidArgumentException::class);
		$this->expectExceptionMessage('Service `other_service` was not registered or could not be autoloaded.');

		$this->serviceLocator->get('other_service');
	}

	public function testHas() {
		$this->serviceLocator->set('service', function() {
			return new stdClass();
		});

		$this->assertTrue($this->serviceLocator->has('service'));
		$this->assertFalse($this->serviceLocator->has('other_service'));
	}
}
