<?php

class SqliteAgeAlgoTest extends FunctionalTestCase {
	/**
	 * @dataProvider getTestCases
	 *
	 *  @param int    $now
	 * @param string $dob
	 * @param int    $expectedAge
	 */
	public function testAgeAlgo($now, $dob, $expectedAge) {
		$stmt = $this->getConnection()->prepare("
            SELECT cast(strftime('%Y.%m%d', :now) - strftime('%Y.%m%d', :dob) as int)
        ");
		$stmt->execute([
            'dob' => $dob,
            'now' => $now
        ]);

		$this->assertEquals($expectedAge, $stmt->fetchColumn());
	}

	/**
	 * @return array
	 */
	public function getTestCases() {
		return [
            ['2018-08-02 00:00:00', '2018-08-02', 0],
            ['2018-08-02 00:00:01', '2018-08-02', 0],
            ['2018-08-01 23:59:59', '2000-08-02', 17],
            ['2018-08-02 00:00:00', '2000-08-02', 18],
        ];
	}
}
