<?php

class AuthUserActionTest extends FunctionalTestCase {
	private $validRequest = [
        'email'    => 'ivan@example.org',
        'password' => '123123',
    ];

	public function testAuth() {
		$this->loadFixture('users.sql');

		$response = $this->sendRequest('POST', '/api/auth.php', $this->validRequest);

		$user = $this->getLastUser();
		$this->assertJsonResponse([
            'result'  => true,
            'message' => '',
            'data'    => [
                'access_token' => $user['access_token'],
            ],
        ], $response);
	}

	/**
	 * @dataProvider getValidationCases
	 *
	 * @param array $requestData
	 * @param array $errors
	 */
	public function testValidation(array $requestData, array $errors) {
		$requestData = array_replace($this->validRequest, $requestData);

		$response = $this->sendRequest('POST', '/api/auth.php', $requestData);

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'Переданы некорректные данные.',
            'data'    => $errors,
        ], $response);
	}

	/**
	 * @return array
	 */
	public function getValidationCases() {
		return [
            [['email' => null], ['email' => 'Адрес электронной почты не долже быть пустым.']],
            [['email' => ''], ['email' => 'Адрес электронной почты не долже быть пустым.']],
            [['email' => str_repeat('a', 256)], ['email' => 'Длина значения не должна превышать 255 символов.']],
            [['email' => 'invalid'], ['email' => 'Значение не является адресом электронной почты.']],

            [['password' => null], ['password' => 'Пароль не должен быть пустым.']],
            [['password' => ''], ['password' => 'Пароль не должен быть пустым.']],
            [['password' => '12345'], ['password' => 'Длина пароля не должна быть меньше 6 символов.']],
            [['password' => str_repeat('a', 256)], ['password' => 'Длина пароля не должна быть больше 255 символов.']],
        ];
	}

	/**
	 * @return array|bool
	 */
	private function getLastUser() {
		$stmt = $this->getConnection()->prepare("
            SELECT   *
            FROM     users
            ORDER BY id DESC
            LIMIT    1
        ");
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
}
