<?php

class DeleteActionTest extends FunctionalTestCase {
	/**
	 * Test successful deletion.
	 */
	public function testDelete() {
		$this->loadFixture('users.sql');

		$response = $this->sendRequest('DELETE', '/api/user.php', [
            'access_token' => 'AAAABBBBCCCCDDDDAAAABBBBCCCCDDDD',
        ]);

		$this->assertJsonResponse([
            'result'  => true,
            'message' => 'The user is deleted.',
            'data'    => [],
        ], $response);

		$stmt = $this->getConnection()->prepare("SELECT id FROM users WHERE access_token = 'AAAABBBBCCCCDDDDAAAABBBBCCCCDDDD'");
		$stmt->execute();
		$this->assertFalse($stmt->fetch());
	}

	/**
	 * No access_token parameters passed to the request.
	 */
	public function testNoAccessTokenProvided() {
		$response = $this->sendRequest('DELETE', '/api/user.php');

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'Authentication required: no access_token parameter provided.',
            'data'    => [],
        ], $response);
	}

	/**
	 * Some nonexistent access_token passed to the request.
	 */
	public function testInvalidAccessToken() {
		$response = $this->sendRequest('DELETE', '/api/user.php', [
            'access_token' => 'invalid',
        ]);

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'Authentication failed: given access token not found.',
            'data'    => [],
        ], $response);
	}
}
