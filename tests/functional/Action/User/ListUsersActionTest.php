<?php

class ListUsersActionTest extends FunctionalTestCase {
	/**
	 * @inheritdoc
	 */
	public function setUp() {
		parent::setUp();
	}

	public function testWithoutFilters() {
		$this->setCurrentTime('2018-05-05 12:00:00');

		$this->addUser('2000-01-01', 1);
		$this->addUser('1990-05-05', 0);

		$response = $this->sendRequest('POST', '/api/list.php');

		$this->assertJsonResponse([
            'result'  => true,
            'message' => '',
            'data'    => [
                [
                    'id'     => '1',
                    'name'   => 'Иванов Иван',
                    'gender' => '1',
                    'age'    => '18',
                ],
                [
                    'id'     => '2',
                    'name'   => 'Иванов Иван',
                    'gender' => '0',
                    'age'    => '28',
                ]
            ]
        ], $response);
	}

	public function testMinAgeFilter() {
		$this->setCurrentTime('2018-05-05 12:00:00');

		$this->addUser('2000-05-05', 1); // 18 Years
	    $this->addUser('2000-05-06', 1); // 17 Years

        $response = $this->sendRequest('POST', '/api/list.php', [
            'age_min' => 18,
        ]);

		$this->assertJsonResponse([
            'result'  => true,
            'message' => '',
            'data'    => [
                [
                    'id'     => '1',
                    'name'   => 'Иванов Иван',
                    'gender' => '1',
                    'age'    => '18',
                ]
            ]
        ], $response);
	}

	public function testMaxAgeFilter() {
		$this->setCurrentTime('2018-05-05 12:00:00');

		$this->addUser('2000-05-05', 1); // 18 Years
        $this->addUser('2000-05-06', 1); // 17 Years

        $response = $this->sendRequest('POST', '/api/list.php', [
            'age_max' => 17,
        ]);

		$this->assertJsonResponse([
            'result'  => true,
            'message' => '',
            'data'    => [
                [
                    'id'     => '2',
                    'name'   => 'Иванов Иван',
                    'gender' => '1',
                    'age'    => '17',
                ]
            ]
        ], $response);
	}

	public function testGenderFilter() {
		$this->setCurrentTime('2018-05-05 12:00:00');

		$this->addUser('2000-05-05', 2);
		$this->addUser('2000-05-06', 1);

		$response = $this->sendRequest('POST', '/api/list.php', [
            'gender' => 2,
        ]);

		$this->assertJsonResponse([
            'result'  => true,
            'message' => '',
            'data'    => [
                [
                    'id'     => '1',
                    'name'   => 'Иванов Иван',
                    'gender' => '2',
                    'age'    => '18',
                ]
            ]
        ], $response);
	}

	public function testEmptyCollection() {
		$response = $this->sendRequest('POST', '/api/list.php', [
            'gender' => 4,
        ]);

		$this->assertJsonResponse([
            'result'  => true,
            'message' => '',
            'data'    => [],
        ], $response);
	}

	/**
	 * Adds user with given parameters.
	 *
	 * @param string $dob
	 * @param int    $gender
	 */
	private function addUser($dob, $gender) {
		$accessToken = bin2hex(openssl_random_pseudo_bytes(16));

		$stmt = $this->getConnection()->prepare("
            INSERT INTO users (name, email, password, dob, gender, phone, created_at, created_ip, access_token)
            VALUES ('Иванов Иван', :email, 'b5d838cdf02502398ded9b215bb04174', :dob, :gender, '+7 (000) 111-22-33', CURRENT_TIMESTAMP, '127.0.0.1', :access_token)
        ");
		$stmt->execute([
		    'dob'                => $dob,
            'gender'       => $gender,
            'access_token' => $accessToken,
            'email'        => $accessToken . '@example.org',
        ]);
	}
}
