<?php

class UpdateUserActionTest extends FunctionalTestCase {
	/**
	 * @var array
	 */
	private $updateData = [
        'access_token' => 'AAAABBBBCCCCDDDDAAAABBBBCCCCDDDD',
        'name'         => 'Сергей',
        'password'     => 'somepass',
        'email'        => 'sergey@example.org',
        'dob'          => '1980-01-01',
        'gender'       => '2',
        'phone'        => '+7 (000) 555-55-55',
    ];

	/**
	 * Test successful user update.
	 */
	public function testUpdate() {
		$this->loadFixture('users.sql');

		$response = $this->sendRequest('PUT', '/api/user.php', $this->updateData);

		$this->assertJsonResponse([
		    'result'        => true,
            'message' => 'The user is updated.',
            'data'    => [],
        ], $response);

		$stmt = $this->getConnection()->prepare("
            SELECT *
            FROM   users
            WhERE  access_token = 'AAAABBBBCCCCDDDDAAAABBBBCCCCDDDD'
        ");
		$stmt->execute();

		$this->assertArraySubset([
            'name'   => 'Сергей',
            'email'  => 'sergey@example.org',
            'dob'    => '1980-01-01',
            'gender' => '2',
            'phone'  => '+7 (000) 555-55-55',
        ], $stmt->fetch(\PDO::FETCH_ASSOC));
	}

	/**
	 * Tests request with access_token.
	 */
	public function testTokenNotProvided() {
		$data = $this->updateData;
		unset($data['access_token']);

		$response = $this->sendRequest('PUT', '/api/user.php', $data);

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'Authentication required: no access_token parameter provided.',
            'data'    => [],
        ], $response);
	}

	/**
	 * Tests invalid access_token.
	 */
	public function testInvalidToken() {
		$data                 = $this->updateData;
		$data['access_token'] = 'invalid';

		$response = $this->sendRequest('PUT', '/api/user.php', $data);

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'Authentication failed: given access token not found.',
            'data'    => [],
        ], $response);
	}
}
