<?php

class CreateUserActionTest extends FunctionalTestCase {
	private $validRequest = [
        'name'     => 'Иван',
        'password' => '123123',
        'email'    => 'ivan@example.org',
        'dob'      => '1999-01-01',
        'gender'   => '0',
        'phone'    => '+7 (000) 111-22-33',
    ];

	public function testCreate() {
		$response = $this->sendRequest('POST', '/api/user.php', $this->validRequest);
		$user     = $this->getLastUser();

		$this->assertNotFalse($user);
		$this->assertEquals([
            'id'           => $user['id'],
            'name'         => 'Иван',
            'password'     => $user['password'],
            'email'        => 'ivan@example.org',
            'dob'          => '1999-01-01',
            'gender'       => '0',
            'phone'        => '+7 (000) 111-22-33',
            'created_at'   => $user['created_at'],
            'created_ip'   => '127.0.0.1',
            'access_token' => $user['access_token'],
        ], $user);

		$this->assertJsonResponse([
		    'result'        => true,
            'message' => '',
            'data'    => [
                'access_token' => $user['access_token'],
            ]
        ], $response);
	}

	public function testUserEmailDuplication() {
		$this->loadFixture('users.sql');

		$response = $this->sendRequest('POST', '/api/user.php', $this->validRequest);

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'Переданы некорректные данные.',
            'data'    => [
                'email' => 'Пользователь с таким адресом электронной почты уже зарегистрирован.',
            ],
        ], $response);
	}

	/**
	 * @dataProvider getValidationCases
	 *
	 * @param array $requestData
	 * @param array $errors
	 */
	public function testValidation(array $requestData, array $errors) {
		$requestData = array_replace($this->validRequest, $requestData);

		$response = $this->sendRequest('POST', '/api/user.php', $requestData);

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'Переданы некорректные данные.',
            'data'    => $errors,
        ], $response);
	}

	/**
	 * @return array
	 */
	public function getValidationCases() {
		return [
		    [['name' => null], ['name' => 'Имя пользователя не должно быть пустым.']],
		    [['name' => ''], ['name' => 'Имя пользователя не должно быть пустым.']],
		    [['name' => str_repeat('a', 256)], ['name' => 'Длина значения не может превышать 255 символов.']],

            [['password' => null], ['password' => 'Пароль не должен быть пустым.']],
            [['password' => ''], ['password' => 'Пароль не должен быть пустым.']],
            [['password' => '12345'], ['password' => 'Длина пароля не должна быть меньше 6 символов.']],
            [['password' => str_repeat('a', 256)], ['password' => 'Длина пароля не должна быть больше 255 символов.']],

            [['email' => null], ['email' => 'Адрес электронной почты не долже быть пустым.']],
            [['email' => ''], ['email' => 'Адрес электронной почты не долже быть пустым.']],
            [['email' => str_repeat('a', 256)], ['email' => 'Длина значения не должна превышать 255 символов.']],
            [['email' => 'invalid'], ['email' => 'Значение не является адресом электронной почты.']],

            [['dob' => null], ['dob' => 'Дата рождения не передана.']],
            [['dob' => ''], ['dob' => 'Дата рождения не передана.']],
            [['dob' => 'invalid'], ['dob' => 'Дата рождения должна быть в формате YYYY-MM-DD (1995-10-10)']],
            [['dob' => (new \DateTime('tomorrow'))->format('Y-m-d')], ['dob' => 'Дата рождения не может быть в будущем.']],

            [['gender' => null], ['gender' => 'Пол не может быть пустым.']],
            [['gender' => ''], ['gender' => 'Пол не может быть пустым.']],
            [['gender' => 'asd'], ['gender' => 'Пол должен быть целочисленным типом.']],
            [['gender' => '4'], ['gender' => 'Пол может принимать только значения: 0, 1, 2.']],

            [['phone' => str_repeat('a', 21)], ['phone' => 'Длина номера телефона не может превышать 20 символов.']],
        ];
	}

	/**
	 * @return array|bool
	 */
	private function getLastUser() {
		$stmt = $this->getConnection()->prepare("
            SELECT   *
            FROM     users
            ORDER BY id DESC
            LIMIT    1
        ");
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
}
