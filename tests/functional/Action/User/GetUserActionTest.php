<?php

class GetUserActionTest extends FunctionalTestCase {
	/**
	 * @inheritdoc
	 */
	public function setUp() {
		parent::setUp();
	}

	/**
	 * Test found user data.
	 */
	public function testGet() {
		$this->loadFixture('users.sql');

		$response = $this->sendRequest('GET', '/api/user.php', [
            'access_token' => 'AAAABBBBCCCCDDDDAAAABBBBCCCCDDDD',
        ]);

		$this->assertJsonResponse([
            'result'  => true,
            'message' => '',
            'data'    => [
                'name'   => 'Иванов Иван',
                'dob'    => '1999-01-01',
                'gender' => '1',
                'phone'  => '+7 (000) 111-22-33',
            ],
        ], $response);
	}

	/**
	 * No access_token value passed in the request.
	 */
	public function testTokenNotProvided() {
		$response = $this->sendRequest('GET', '/api/user.php');

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'No access_token parameter provided.',
            'data'    => [],
        ], $response);
	}

	/**
	 * Empty string passed as the token value.
	 */
	public function testEmptyString() {
		$response = $this->sendRequest('GET', '/api/user.php', [
            'access_token' => '',
        ]);

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'No access_token parameter provided.',
            'data'    => [],
        ], $response);
	}

	/**
	 * No users in DB with given access token found.
	 */
	public function testInvalidAccessToken() {
		$response = $this->sendRequest('GET', '/api/user.php', [
            'access_token' => 'invalid',
        ]);

		$this->assertJsonResponse([
            'result'  => false,
            'message' => 'User with given access token not found.',
            'data'    => [],
        ], $response);
	}
}
