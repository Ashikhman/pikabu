<?php

use PHPUnit\Framework\TestCase;
use Pikabu\Framework\FrontController;
use Pikabu\Framework\Http\Request;
use Pikabu\Framework\Http\Response;

/**
 * Support class for functional testing.
 */
class FunctionalTestCase extends TestCase {
	/**
	 * @var FrontController
	 */
	private $frontController;

	/**
	 * @var bool
	 */
	private $migrationsExecuted = false;

	/**
	 * @inheritdoc
	 */
	public function setUp() {
		$this->frontController = new FrontController(true);
		$this->frontController->init();

		$this->executeMigrations();
		$this->clearDb();

		timecop_return();
	}

	/**
	 * Set current time to provided one.
	 *
	 * @param string $datetime
	 */
	protected function setCurrentTime($datetime) {
		timecop_travel(strtotime($datetime));
	}

	/**
	 * Asserts that the given response contains json data and has corresponding header.
	 *
	 * @param array    $expectedContent
	 * @param Response $response
	 */
	protected function assertJsonResponse($expectedContent, Response $response) {
		$this->assertSame('application/json', $response->headers->get('Content-Type', ''));
		$this->assertJson($response->getContent());

		$this->assertSame($expectedContent, json_decode($response->getContent(), true));
	}

	/**
	 * Makes request,
	 *
	 * @param string $method
	 * @param string $uri
	 * @param array  $params
	 *
	 * @return Response
	 */
	protected function sendRequest($method, $uri, array $params = []) {
		$server = [
			'REQUEST_URI'          => $uri,
			'REQUEST_METHOD'       => strtoupper($method),
            'REMOTE_ADDR' => '127.0.0.1',
		];

		if (in_array(strtoupper($method), [Request::METHOD_POST, Request::METHOD_PUT])) {
			$request = new Request($server, [], $params);
		} else {
			$request = new Request($server, $params, []);
		}

		return $this->frontController->handle($request);
	}

	/**
	 * Loads data from the given fixture file.
	 *
	 * @param string $filename Relatives to tests/fixtures dir.
	 */
	protected function loadFixture($filename) {
		$filePath = __DIR__ . '/../fixtures/' . $filename;
		if (!file_exists($filePath)) {
			throw new \InvalidArgumentException("Fixture `{$filename}` not found in path `$filePath`.");
		}

		$this->getConnection()->prepare(file_get_contents($filePath))->execute();
	}

	/**
	 * Returns pdo connection.
	 *
	 * @return PDO
	 */
	protected function getConnection() {
		return $this->frontController->getServiceLocator()->get('connection');
	}

	/**
	 * Executes project migrations one time during tests.
	 */
	private function executeMigrations() {
		if ($this->migrationsExecuted) {
			return;
		}

		$pattern = $this->frontController->getProjectDir() . '/migrations/*.sql';
		foreach (glob($pattern) as $filename) {
			$this->getConnection()->exec(file_get_contents($filename));
		}
	}

	/**
	 * Clears database data.
	 */
	private function clearDb() {
		$this->getConnection()->exec('DELETE FROM users');
	}
}
