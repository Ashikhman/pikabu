Развертывание в докере
-----------

1. Скопировать файл .env.dist в .env и настроить значения переменных окружения.
2. Запуск проекта `docker-compose up -d`
3. Создание БД и выполнение миграций `bin/docker-createdb`
4. Отправить запрос на `http://localhost:8081` (8081 - значение переменной NGINX_PORT в .env) и убедиться что получен ответ.

Развертывание на хосте
-----------

1. Установить php5.6 с расширением sqlite3.
2. Настроить nginx:

        server {
            listen 80 default_server;
            listen [::]:80 default_server;
    
            root /app/public;
    
            location / {
                try_files $uri /index.php$is_args$args;
            }
    
            location = /index.php {
                fastcgi_pass php:9000;
                fastcgi_split_path_info ^(.+\.php)(/.*)$;
                include fastcgi_params;
                fastcgi_read_timeout 30;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            }
    
            error_log /var/log/nginx/error.log;
            access_log /var/log/nginx/access.log;
        }
        
где `/app/public` - папка с исходными кодами.
3. Создать БД и запустить миграции `php /app/console/createdb.php`

Развертывание для разработчика в docker
----------

1. Скопировать файл .env.dist в .env и настроить значения переменных окружения.
2. Запуск проекта `docker-compose up -d`
3. Запуск php-cs-fixer `bin/docker-csfix`
4. Запуск phpstan `bin/docker-phpstan`
5. Запуск тестов `bin/docker-test`
6. Настройка PHPStorm для разработки через TDD:
    1. Add docker CLI interpreter with `pikabu_test:latest` image selected.
    2. Add PhpUnit as test framework and select default configuration file.
    3. Select test file and press ctrl + shift + F10.
