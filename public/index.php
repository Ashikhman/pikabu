<?php

use Pikabu\Framework\FrontController;
use Pikabu\Framework\Http\Request;

require_once __DIR__ . '/../src/autoload.php';

$frontController = new FrontController(false);
$response = $frontController->handle(Request::createFromGlobals());
$response->send();
