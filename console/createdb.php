<?php

use Pikabu\Framework\FrontController;

require_once dirname(__DIR__) . '/src/autoload.php';

$frontController = new FrontController(true);
$frontController->init();

/** @var PDO $pdo */
$pdo = $frontController->getServiceLocator()->get('connection');

$pattern = dirname(__DIR__) . '/migrations/*.sql';
foreach (glob($pattern) as $filename) {
    $pdo->exec(file_get_contents($filename));
}